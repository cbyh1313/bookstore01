package com.cyzz.bookstore.modules.mapper;

import com.cyzz.bookstore.modules.model.Book;
import com.cyzz.bookstore.common.base.MyBaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
public interface BookMapper extends MyBaseMapper<Book> {

}