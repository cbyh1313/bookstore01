package com.cyzz.bookstore.modules.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.cyzz.bookstore.common.myException.DBException;
import com.cyzz.bookstore.modules.model.User;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
public interface IUserService extends IService<User> {
    /**
     * 查询所有用户
     * @return
     * @throws Exception
     */
    List<User> selectAllUser()throws Exception;

    /**
     * 模糊分页查询用户
     * @param page
     * @param value
     * @return
     * @throws Exception
     */
    Page<User> selectLike(Page<User> page, String value)throws Exception;

    /**
     * 分页查询用户
     * @param page
     * @return
     * @throws Exception
     */
    Page<User> selectUserPage(Page<User> page)throws Exception;
    User selectUserByUsername(String username)throws DBException;
    User selectUserById(Long id)throws DBException;
    List<User> selectUserByIds(Long[] ids) throws DBException;
    Integer insertUser(User user)throws DBException;
    Integer updateUser(User user)throws DBException;
    Page<User> selectStarPage(Page<User> page, Long userId) throws Exception;

    /**
     * 根据用户类型查找用户
     * @param userType
     * @return
     * @throws DBException
     */
    List<User> selectUsersByUserType(Integer userType)throws DBException;

}
