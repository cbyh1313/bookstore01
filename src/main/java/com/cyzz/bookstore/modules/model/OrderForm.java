package com.cyzz.bookstore.modules.model;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
@TableName("order_form")
public class OrderForm extends Model<OrderForm> {

    private static final long serialVersionUID = 1L;

    @TableId("order_number")
	private Long orderNumber;
	@TableField("client_id")
	private Long clientId;
    /**
     * 订单编号
     */
	@TableField("store_id")
	private Long storeId;
	@TableField("book_id")
	private Long bookId;
	@TableField("purchase_date")
	private Date purchaseDate;
	@TableField("purchase_count")
	private Integer purchaseCount;
    /**
     * 单价
     */
	private Double price;
    /**
     * 订单是否处理（0：否，1：是）
     */
	@TableField("is_deal")
	private Integer isDeal;


	public Long getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Long orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public Integer getPurchaseCount() {
		return purchaseCount;
	}

	public void setPurchaseCount(Integer purchaseCount) {
		this.purchaseCount = purchaseCount;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getIsDeal() {
		return isDeal;
	}

	public void setIsDeal(Integer isDeal) {
		this.isDeal = isDeal;
	}

	@Override
	protected Serializable pkVal() {
		return this.orderNumber;
	}

	@Override
	public String toString() {
		return "OrderForm{" +
			"orderNumber=" + orderNumber +
			", clientId=" + clientId +
			", storeId=" + storeId +
			", bookId=" + bookId +
			", purchaseDate=" + purchaseDate +
			", purchaseCount=" + purchaseCount +
			", price=" + price +
			", isDeal=" + isDeal +
			"}";
	}
}
