package com.cyzz.bookstore.modules.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author CBY
 * @since 2018-06-04
 */
public class Store extends Model<Store> {

    private static final long serialVersionUID = 1L;

    @TableId("store_id")
	private Long storeId;
	@TableField("user_id")
	private Long userId;
    /**
     * 店名
     */
	@TableField("store_name")
	private String storeName;
    /**
     * 经营范围
     */
	@TableField("business_scope")
	private String businessScope;
    /**
     * 发货地址
     */
	@TableField("store_address")
	private String storeAddress;


	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getBusinessScope() {
		return businessScope;
	}

	public void setBusinessScope(String businessScope) {
		this.businessScope = businessScope;
	}

	public String getStoreAddress() {
		return storeAddress;
	}

	public void setStoreAddress(String storeAddress) {
		this.storeAddress = storeAddress;
	}

	@Override
	protected Serializable pkVal() {
		return this.storeId;
	}

	@Override
	public String toString() {
		return "Store{" +
			"storeId=" + storeId +
			", userId=" + userId +
			", storeName=" + storeName +
			", businessScope=" + businessScope +
			", storeAddress=" + storeAddress +
			"}";
	}
}
