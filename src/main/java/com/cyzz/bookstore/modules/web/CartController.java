package com.cyzz.bookstore.modules.web;


import com.baomidou.mybatisplus.plugins.Page;
import com.cyzz.bookstore.common.base.MyBaseController;
import com.cyzz.bookstore.common.json.CartVO;
import com.cyzz.bookstore.modules.model.Book;
import com.cyzz.bookstore.modules.model.Cart;
import com.cyzz.bookstore.modules.model.OrderForm;
import com.cyzz.bookstore.modules.model.User;
import com.cyzz.bookstore.modules.service.IBookService;
import com.cyzz.bookstore.modules.service.ICartService;
import com.cyzz.bookstore.modules.service.IOrderFormService;
import com.cyzz.bookstore.modules.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
@Controller
@RequestMapping("/cart")
public class CartController extends MyBaseController {
    @Autowired
    private ICartService cartService;
    @Autowired
    private IBookService bookService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IOrderFormService orderFormService;

    /**
     * 获取顾客的购物车消息
     * @param userId
     * @return
     */
    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    @ResponseBody
    public Object getCart( @RequestParam("userId") Long userId)  {
        List<Cart> list = cartService.selectCartByUserId(userId);
        List<CartVO.GetWholeCart> cart = new ArrayList<>();
        for(Cart c :list){
            Book b = bookService.selectBookById(c.getBookId());//已知要买书的id
            User u = userService.selectUserById(b.getBookstoreId());
//            Cart ccc = cartService.selectById(userId);
            CartVO.GetWholeCart wholeCart = new CartVO.GetWholeCart();
            wholeCart.setCartid(c.getCartId());
            wholeCart.setBookid(c.getBookId());
            wholeCart.setImageUrl(b.getImageUrl());
            wholeCart.setTitle(b.getTitle());
            wholeCart.setPrice(b.getPrice());
            wholeCart.setAuthor(b.getAuthor());
            wholeCart.setBookStore(u.getUserName());
            wholeCart.setNumber(c.getPurchaseCount());
            cart.add(wholeCart);
        }
        return renderSuccess(cart);
    }

    /**
     * 单条数据加入购物车
     * @param form
     * @return
     */
    @RequestMapping(value = "/addtocart", method = RequestMethod.POST)
    @ResponseBody
    public Object addtocart( @RequestBody CartVO.AddCart form) {
        Long userid = form.getUserid();
        Long bookid = form.getBookid();
        List<Cart> list = cartService.selectCartByUserId(userid);
        for(Cart c:list){
            System.out.println(bookid+" "+c.getBookId());
            if(c.getBookId().equals(bookid) ) {//该用户的购物车中存在该书
//                System.out.println(bookid+"cart");
                c.setPurchaseCount(c.getPurchaseCount() + 1);
                return renderSuccess();
            }
        }
        Long cart_id = new Date().getTime();
        Integer purchase_count = 1;
        Cart cart  = new Cart();
        cart.setCartId(cart_id);
        cart.setUserId(userid);
        cart.setBookId(bookid);
        cart.setPurchaseCount(purchase_count);
        cartService.insertCart(cart);
        return renderSuccess();
    }

    /**
     * 保存顾客的购物车
     * @param form
     * @return
     */
    @RequestMapping(value = "/savecart", method = RequestMethod.POST)
    @ResponseBody
    public Object savecart( @RequestBody CartVO.SaveCart form) {
        List<CartVO.CartForSave> saveList = form.getBaskets();
        for(CartVO.CartForSave c: saveList){
            Cart cart = cartService.selectById(c.getCartid());
            cart.setPurchaseCount(c.getNumber());
            cartService.updateCate(cart);
        }
        return renderSuccess();
    }

    /**
     * 删除购物车
     * @param cartid
     * @return
     */
    @RequestMapping(value = "/deleteCart", method = RequestMethod.GET)
    @ResponseBody
    public Object deleteCart(@RequestParam("cartid") Long cartid ) {
        cartService.delectCartByCartId(cartid);
        return renderSuccess();
    }

    /**
     * 购物车结算
     * @return
     */
    @RequestMapping(value = "/buyAll", method = RequestMethod.GET)
    @ResponseBody
    public Object buyAll( @RequestParam("userid") Long userid) {
//        System.out.println(userid);
        List<Cart> list = cartService.selectCartByUserId(userid);
        for(Cart c:list){
            Book book  = bookService.selectBookById(c.getBookId());
            Long storeid = book.getBookstoreId();
            OrderForm orderForm  = new OrderForm();
            orderForm.setOrderNumber(new Date().getTime());
            orderForm.setPurchaseDate(new Date());
            orderForm.setBookId(c.getBookId());
            orderForm.setClientId(c.getUserId());
            orderForm.setIsDeal(0);
            orderForm.setPrice(book.getPrice());
            orderForm.setPurchaseCount(c.getPurchaseCount());
            orderForm.setStoreId(storeid);
            orderFormService.insertOrderForm(orderForm);//添加进去订单
            cartService.delectCartByCartId(c.getCartId());//删除购物车记录
        }
        return renderSuccess();
    }
}
