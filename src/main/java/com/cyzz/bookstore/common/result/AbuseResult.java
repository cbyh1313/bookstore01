package com.cyzz.bookstore.common.result;

import java.util.Date;

/**
 * Created by 13296 on 2017/8/31.
 */
public class AbuseResult {
    private Long id;//举报表id
    private String informant;//被举报人
    private String informer;//举报者
    private Date informTime;//举报时间
    private Long targetId;//目标id
    private int type;//举报类型 0:图片，1：评论
    private int result;//审核结果 0:未审核 1：通过 2：拒绝

    public AbuseResult() {

    }

    public AbuseResult(Long id, String informant, String informer, Date informTime, Long targetId, int type, int result) {
        this.id = id;
        this.informant = informant;
        this.informer = informer;
        this.informTime = informTime;
        this.targetId = targetId;
        this.type = type;
        this.result = result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInformant() {
        return informant;
    }

    public void setInformant(String informant) {
        this.informant = informant;
    }

    public String getInformer() {
        return informer;
    }

    public void setInformer(String informer) {
        this.informer = informer;
    }

    public Date getInformTime() {
        return informTime;
    }

    public void setInformTime(Date informTime) {
        this.informTime = informTime;
    }

    public Long getTargetId() {
        return targetId;
    }

    public void setTargetId(Long targetId) {
        this.targetId = targetId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}
