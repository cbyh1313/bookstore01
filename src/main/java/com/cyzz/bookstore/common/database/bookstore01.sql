/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : bookstore

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-06-04 00:41:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `book_id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL COMMENT '书名',
  `intro` varchar(255) DEFAULT NULL COMMENT '简介',
  `type` varchar(255) NOT NULL COMMENT '图书类型',
  `bookstore_id` bigint(20) NOT NULL COMMENT '书店名',
  `image_url` varchar(255) NOT NULL COMMENT '图片链接',
  `price` double(10,2) NOT NULL COMMENT '售价',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `publisher` varchar(255) DEFAULT NULL COMMENT '出版社',
  `publish_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `sales_volume` int(11) DEFAULT '0' COMMENT '销售量',
  `stock` int(11) DEFAULT NULL COMMENT '库存',
  `is_show` int(11) DEFAULT '0' COMMENT '是否上架（0：否，1：是）',
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES ('1', '《Java语言应用开发基础》', '通俗易懂', 'IT', '1', 'http://unicover.duxiu.com/coverNew/CoverNew.dll?iid=6969676F686867706B705E9CADB0A1AD5E693134313339393038', '38.00', '柳西玲等编著 ', '清华大学出版社 ', '2018-06-02 16:20:42', '0', '100', '1');
INSERT INTO `book` VALUES ('2', '《Java高级编程》', '通俗易懂', 'IT', '1', 'http://unicover.duxiu.com/coverNew/CoverNew.dll?iid=6A6D686A6F6968726F6E5F9DAEB1A2AE5F6A3238313738393639', '10.00', '魏勇 ', '清华大学出版社 ', '2018-05-31 00:20:20', '0', '1000', '1');

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `cart_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `book_id` bigint(20) NOT NULL,
  `purchase_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cart
-- ----------------------------

-- ----------------------------
-- Table structure for commet
-- ----------------------------
DROP TABLE IF EXISTS `commet`;
CREATE TABLE `commet` (
  `comment_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `book_id` bigint(20) NOT NULL,
  `star` int(11) DEFAULT '3' COMMENT '0-5星级',
  `context` varchar(255) DEFAULT NULL COMMENT '评论内容',
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of commet
-- ----------------------------

-- ----------------------------
-- Table structure for order_form
-- ----------------------------
DROP TABLE IF EXISTS `order_form`;
CREATE TABLE `order_form` (
  `order_number` bigint(20) NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `store_id` bigint(20) NOT NULL COMMENT '订单编号',
  `book_id` bigint(20) NOT NULL,
  `purchase_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `purchase_count` int(11) DEFAULT '1',
  `price` double(10,2) DEFAULT NULL COMMENT '单价',
  `is_deal` int(11) DEFAULT '0' COMMENT '订单是否处理（0：否，1：是）',
  PRIMARY KEY (`order_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order_form
-- ----------------------------

-- ----------------------------
-- Table structure for store
-- ----------------------------
DROP TABLE IF EXISTS `store`;
CREATE TABLE `store` (
  `store_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `store_name` varchar(255) NOT NULL DEFAULT '' COMMENT '店名',
  `business_scope` varchar(255) DEFAULT NULL COMMENT '经营范围',
  `store_address` varchar(255) DEFAULT NULL COMMENT '发货地址',
  PRIMARY KEY (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of store
-- ----------------------------

-- ----------------------------
-- Table structure for UserForKD
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `passwd` varchar(255) NOT NULL DEFAULT '0' COMMENT '0:顾客\r\n1：店主\r\n2：管理员\r\n',
  `user_type` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of UserForKD
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'cby', '13760693464', '123456', '0', '广东湛江');
INSERT INTO `user` VALUES ('2', 'yq', '13416141472', '123456', '0', '广东湛江');
