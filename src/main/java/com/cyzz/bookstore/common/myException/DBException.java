package com.cyzz.bookstore.common.myException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Y
 * Date: 2017-08-28
 * Time: 14:02
 * 分享插入失败:SHARE_INSERT_FAILURE ;
 * 分享删除失败:SHARE_DELETE_FAILURE ;
 * 审核提交失败:EXAMINE_INSERT_FAILURE ;
 * 赞操作失败:PRAISE_SELECT_FAILURE ;
 * 不感兴趣操作失败:HIDE_UPDATE_FAILURE ;
 * 收藏操作失败:COLLECT_UPDATE_FAILURE ;
 * 关注操作失败:STAR_UPDATE_FAILURE ;
 * 评论插入失败:COMMENT_INSERT_FAILURE ;
 */
public class DBException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public static final int SHARE_SELECT_FAILURE = 10;
    public static final int SHARE_INSERT_FAILURE = 11;
    public static final int SHARE_UPDATE_FAILURE = 12;

    public static final int PRAISE_SELECT_FAILURE = 20;
    public static final int PRAISE_INSERT_FAILURE = 21;
    public static final int PRAISE_UPDATE_FAILURE = 22;

    public static final int HIDE_SELECT_FAILURE = 30;
    public static final int HIDE_INSERT_FAILURE = 31;
    public static final int HIDE_UPDATE_FAILURE = 32;

    public static final int COLLECT_SELECT_FAILURE = 40;
    public static final int COLLECT_INSERT_FAILURE = 41;
    public static final int COLLECT_UPDATE_FAILURE = 42;

    public static final int EXAMINE_SELECT_FAILURE = 50;
    public static final int EXAMINE_INSERT_FAILURE = 51;
    public static final int EXAMINE_UPDATE_FAILURE = 52;
            ;
    public static final int USER_SELECT_FAILURE = 60;
    public static final int USER_INSERT_FAILURE = 61;
    public static final int USER_UPDATE_FAILURE = 62;

    public static final int STAR_SELECT_FAILURE = 70;
    public static final int STAR_INSERT_FAILURE = 71;
    public static final int STAR_UPDATE_FAILURE = 72;

    public static final int COMMENT_SELECT_FAILURE = 80;
    public static final int COMMENT_INSERT_FAILURE = 81;
    public static final int COMMENT_UPDATE_FAILURE = 82;

    public static final int ABUSE_SELECT_FAILURE = 90;
    public static final int ABUSE_INSERT_FAILURE = 91;
    public static final int ABUSE_UPDATE_FAILURE = 92;

    public static final int IMAGE_SELECT_FAILURE = 100;
    public static final int IMAGE_INSERT_FAILURE = 101;
    public static final int IMAGE_UPDATE_FAILURE = 102;

    public static final int INFO_SELECT_FAILURE = 110;
    public static final int INFO_INSERT_FAILURE = 111;
    public static final int INFO_UPDATE_FAILURE = 112;


    private int code;

    public int getCode() {
        return code;
    }

    public DBException() {
        super();
    }

    public DBException(int code){
        super();
        this.code = code;
    }

    public DBException(String message) {
        super(message);
    }

    public DBException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
