package com.cyzz.bookstore.common.utils;

import com.cyzz.bookstore.common.myException.DBException;

/**
 * 验证类
 * Created by 13296 on 2017/7/13.
 */
public class AssertThrowUtil {//验证类
    public static String assertNotBlank(String message,String target){
        if(target==null||target.trim().length()==0){
            throw new DBException(message);
        }
        return target;
    }

    public static String $(String message,String target){
        return  assertNotBlank(message,target);
    }

    public static void assertNotNull(String message,Object o){
        if(o==null){
            throw new DBException(message);
        }
    }

    public static void assertNull(String message,Object o){
        if(o!=null){
            throw new DBException(message);
        }
    }

    public static void assertEquals(String message,Object a,Object b){
        if(a==null?a!=b:!a.equals(b)){
            throw new DBException(message);
        }
    }

    public static void assertNotEquals(String message,Object a,Object b){
        if(a==null?a==b:a.equals(b)){
            throw new DBException(message);
        }
    }

}
