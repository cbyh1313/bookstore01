package com.cyzz.bookstore.common.base;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * Created by 13296 on 2017/8/9.
 */
public interface MyBaseMapper<T> extends BaseMapper<T> {
    // 这里可以写自己的公共方法、注意不要让 mp 扫描到误以为是实体 Base 的操作
    
}
