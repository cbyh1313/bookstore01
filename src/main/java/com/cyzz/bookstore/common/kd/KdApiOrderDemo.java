package com.cyzz.bookstore.common.kd;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class KdApiOrderDemo {
    //电商ID
    private String EBusinessID="1349793";
    private String AppKey="6278f7a3-b4ea-4f63-b009-307697c29278";
    //请求url, 测试环境地址：http://testapi.kdniao.cc:8081/api/EOrderService
    private String ReqURL="http://testapi.kdniao.cc:8081/api/Eorderservice";

    public String resultmsg;


    public String sendData(OrderForKD order1, UserForKD sender, UserForKD receiver) throws Exception{
        //System.out.println(str);
        String str = "{'OrderCode': '"+ order1.OrderCode+"'," +
                "'ShipperCode':'SF'," +
                "'PayType':1," +
                "'ExpType':1," +
                "'Cost':10.0," +
                "'OtherCost':0.0," +
                "'Sender':" +
                "{" +
                "'Company':'bookstore','Name':'"+sender.Name+"','Mobile':'"+sender.Mobile+"','ProvinceName':'"+sender.ProvinceName +"','CityName':'"+sender.CityName+"','ExpAreaName':'"+sender.ExpAreaName+"','Address':'"+sender.Address+"'}," +
                "'Receiver':" +
                "{" +
                "'Company':'Person','Name':'"+receiver.Name+"','Mobile':'"+receiver.Mobile+"','ProvinceName':'"+receiver.ProvinceName+"','CityName':'"+receiver.CityName+"','ExpAreaName':'"+receiver.ExpAreaName+"','Address':'"+receiver.Address+"'}," +
                "'Commodity':" +
                "[{" +
                "'GoodsName':'"+order1.GoodsName+"','Goodsquantity':1,'GoodsWeight':1.0}]," +
                "'Weight':1.0," +
                "'Quantity':1," +
                "'Volume':0.0," +
                "'Remark':'"+order1.Remark+"'," +
                "'IsReturnPrintTemplate':1}";
        resultmsg=new KdApiOrderDemo().orderOnlineByJson(str);
//        System.out.println(resultmsg);//返回的数据中包括html代码
        return catchhtml(resultmsg);
    }

    public String catchhtml(String str)throws Exception{
        String[] temp=str.split("<!DOCTYPE html>");
        str="<!DOCTYPE html>"+temp[1];
        str=str.substring(0,str.length()-2);
        str=str.replace("\\r\\n"," ");
        str=str.replace("\\\"","\"");
//        System.out.println(str);

        long date=new Date().getTime();
//        System.out.println(date);
        String basepath = "D:/intelliJ_IDEA/Workspaces/bookstore/src/main/webapp/resources/";
        String pathname=basepath+date+".html";
        //将html代码存入文件
        PrintStream ps = null;
//        File file = null;
        try {
            File file = new File(pathname);
            ps = new PrintStream(new FileOutputStream(file));
//            str = new String(str.getBytes(),"utf-8");
//            str = new String(str.getBytes("ISO-8859-1"),"utf-8");
            ps.println(str);
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally{
            ps.close();
            Thread.sleep(2000);
        }
        String url = "http://localhost:8080/bookstore/resources/"+date+".html";
        return url;
    }



    /**
     * Json方式 电子面单
     * @throws Exception
     */
    public String orderOnlineByJson(String requestData) throws Exception{

        Map<String, String> params = new HashMap<String, String>();
        params.put("RequestData", urlEncoder(requestData, "UTF-8"));
        params.put("EBusinessID", EBusinessID);
        params.put("RequestType", "1007");
        String dataSign=encrypt(requestData, AppKey, "UTF-8");
        params.put("DataSign", urlEncoder(dataSign, "UTF-8"));
        params.put("DataType", "2");

        String result=sendPost(ReqURL, params);

        //根据公司业务处理返回的信息......

        return result;
    }
    /**
     * MD5加密
     * @param str 内容
     * @param charset 编码方式
     * @throws Exception
     */
    @SuppressWarnings("unused")
    private String MD5(String str, String charset) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(str.getBytes(charset));
        byte[] result = md.digest();
        StringBuffer sb = new StringBuffer(32);
        for (int i = 0; i < result.length; i++) {
            int val = result[i] & 0xff;
            if (val <= 0xf) {
                sb.append("0");
            }
            sb.append(Integer.toHexString(val));
        }
        return sb.toString().toLowerCase();
    }

    /**
     * base64编码
     * @param str 内容
     * @param charset 编码方式
     * @throws UnsupportedEncodingException
     */
    private String base64(String str, String charset) throws UnsupportedEncodingException{
        String encoded = Base64.encode(str.getBytes(charset));
        return encoded;
    }

    @SuppressWarnings("unused")
    private String urlEncoder(String str, String charset) throws UnsupportedEncodingException{
        String result = URLEncoder.encode(str, charset);
        return result;
    }

    /**
     * 电商Sign签名生成
     * @param content 内容
     * @param keyValue Appkey
     * @param charset 编码方式
     * @throws UnsupportedEncodingException ,Exception
     * @return DataSign签名
     */
    @SuppressWarnings("unused")
    private String encrypt (String content, String keyValue, String charset) throws UnsupportedEncodingException, Exception
    {
        if (keyValue != null)
        {
            return base64(MD5(content + keyValue, charset), charset);
        }
        return base64(MD5(content, charset), charset);
    }

    /**
     * 向指定 URL 发送POST方法的请求
     * @param url 发送请求的 URL
     * @param params 请求的参数集合
     * @return 远程资源的响应结果
     */
    @SuppressWarnings("unused")
    private String sendPost(String url, Map<String, String> params) {
        OutputStreamWriter out = null;
        BufferedReader in = null;
        StringBuilder result = new StringBuilder();
        try {
            URL realUrl = new URL(url);
            HttpURLConnection conn =(HttpURLConnection) realUrl.openConnection();
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // POST方法
            conn.setRequestMethod("POST");
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("UserForKD-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.connect();
            // 获取URLConnection对象对应的输出流
            out = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            // 发送请求参数
            if (params != null) {
                StringBuilder param = new StringBuilder();
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    if(param.length()>0){
                        param.append("&");
                    }
                    param.append(entry.getKey());
                    param.append("=");
                    param.append(entry.getValue());
//                    System.out.println(entry.getKey()+":"+entry.getValue());
                }
//                System.out.println("param:"+param.toString());
                out.write(param.toString());
            }
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        return result.toString();
    }
}

