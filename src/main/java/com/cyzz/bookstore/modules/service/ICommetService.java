package com.cyzz.bookstore.modules.service;

import com.cyzz.bookstore.common.myException.DBException;
import com.cyzz.bookstore.modules.model.Commet;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
public interface ICommetService extends IService<Commet> {

    List<Commet> selectCommetByBookId(Long bookId) throws DBException;
    List<Integer> selectCountByStar(Long bookId) throws DBException;
    Integer insertCommet(Commet commet) throws DBException;
    Integer updateCommet(Commet commet)throws DBException;
    Commet selectCommetByBookIdAndUserId(Long bookId,Long userId)throws DBException;
	
}
