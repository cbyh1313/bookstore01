package com.cyzz.bookstore.modules.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
public class Commet extends Model<Commet> {

    private static final long serialVersionUID = 1L;

	public Commet(Long commentId, Long userId, Long bookId, Integer star, String context) {
		this.commentId = commentId;
		this.userId = userId;
		this.bookId = bookId;
		this.star = star;
		this.context = context;
	}

	public Commet(){

	}

	@TableId("comment_id")
	private Long commentId;
	@TableField("user_id")
	private Long userId;
	@TableField("book_id")
	private Long bookId;
    /**
     * 0-5星级
     */
	private Integer star;
    /**
     * 评论内容
     */
	private String context;


	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public Integer getStar() {
		return star;
	}

	public void setStar(Integer star) {
		this.star = star;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	@Override
	protected Serializable pkVal() {
		return this.commentId;
	}

	@Override
	public String toString() {
		return "Commet{" +
			"commentId=" + commentId +
			", userId=" + userId +
			", bookId=" + bookId +
			", star=" + star +
			", context=" + context +
			"}";
	}
}
