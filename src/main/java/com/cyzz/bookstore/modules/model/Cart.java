package com.cyzz.bookstore.modules.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
public class Cart extends Model<Cart> {

    private static final long serialVersionUID = 1L;

    @TableId("cart_id")
	private Long cartId;
	@TableField("user_id")
	private Long userId;
	@TableField("book_id")
	private Long bookId;
	@TableField("purchase_count")
	private Integer purchaseCount;

	public Cart() {
	}

	public Cart(Long cartId, Long userId, Long bookId, Integer purchaseCount) {
		this.cartId = cartId;
		this.userId = userId;
		this.bookId = bookId;
		this.purchaseCount = purchaseCount;
	}

	public Long getCartId() {
		return cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public Integer getPurchaseCount() {
		return purchaseCount;
	}

	public void setPurchaseCount(Integer purchaseCount) {
		this.purchaseCount = purchaseCount;
	}

	@Override
	protected Serializable pkVal() {
		return this.cartId;
	}

	@Override
	public String toString() {
		return "Cart{" +
			"cartId=" + cartId +
			", userId=" + userId +
			", bookId=" + bookId +
			", purchaseCount=" + purchaseCount +
			"}";
	}
}
