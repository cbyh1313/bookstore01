package com.cyzz.bookstore.modules.mapper;

import com.cyzz.bookstore.common.base.MyBaseMapper;
import com.cyzz.bookstore.modules.model.Store;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author CBY
 * @since 2018-06-04
 */
public interface StoreMapper extends MyBaseMapper<Store> {

}