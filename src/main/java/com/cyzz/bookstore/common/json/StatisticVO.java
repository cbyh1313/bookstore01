package com.cyzz.bookstore.common.json;

import com.cyzz.bookstore.modules.model.Book;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by 13296 on 2018/6/20.
 * 统计
 */
public class StatisticVO {
    /**
     * 顾客统计数据
     */
    public static class Client{
        private Integer classics=0;// “经典名著”：10，
        private Integer IT=0;//“计算机与互联网”：20，
        private Integer relaxation=0;//“娱乐休闲”：20，
        private Integer science=0;//“科普读物”：5，
        private Integer life=0;// “生活图书”：1，
        private Integer language=0;// “外语学习”：2
        private List<BooksVO.getBookList> books;

        public Integer getClassics() {
            return classics;
        }

        public void setClassics(Integer classics) {
            this.classics = classics;
        }

        public Integer getIT() {
            return IT;
        }

        public void setIT(Integer IT) {
            this.IT = IT;
        }

        public Integer getRelaxation() {
            return relaxation;
        }

        public void setRelaxation(Integer relaxation) {
            this.relaxation = relaxation;
        }

        public Integer getScience() {
            return science;
        }

        public void setScience(Integer science) {
            this.science = science;
        }

        public Integer getLife() {
            return life;
        }

        public void setLife(Integer life) {
            this.life = life;
        }

        public Integer getLanguage() {
            return language;
        }

        public void setLanguage(Integer language) {
            this.language = language;
        }

        public List<BooksVO.getBookList> getBooks() {
            return books;
        }

        public void setBooks(List<BooksVO.getBookList> books) {
            this.books = books;
        }

        public void setSums(int[] sums){
            if(sums.length==6){
                this.classics=sums[0];
                this.IT = sums[1];
                this.relaxation=sums[2];
                this.science = sums[3];
                this.life=sums[4];
                this.language = sums[5];
            }
        }

        /**
         * 获取购买最多的类型
         * @param sums
         * @return
         */
        public String getMaxType(int[] sums){
            int index = 0;
            for(int i=1;i<6;i++){
                if(sums[index]<sums[i]){
                    index=i;
                }
            }
            switch (index){
                case 0:return "经典名著";
                case 1:;return "计算机与互联网";
                case 2:;return "娱乐休闲";
                case 3:;return "科普读物";
                case 4:;return "生活图书";
                case 5:;return "外语学习";
                default:return "经典名著";
            }
        }


    }

    /**
     * 店主统计数据
     */
    public static class ShopKeeper{
        private Double totalMoney = 0.0;//店铺的总营业额,
        private List<BooksVO.BookMap> books = new ArrayList<>();//[{title:”书1”,num:”销售量”},{title:”书2”,num:”销售量”},{title:”书3”,num:”销售量”}] //返回该店铺销售量前3的书本名称和销售量
        private double[] seasonMoney = new double[4];//[第一季度，第二季度，第三季度，第四季度] //返回该店铺季度营业额，按顺序返回一个数组

        public Double getTotalMoney() {
            return totalMoney;
        }

        public void setTotalMoney(Double totalMoney) {
            this.totalMoney = totalMoney;
        }

        public List<BooksVO.BookMap> getBooks() {
            return books;
        }

        public void setBooks(List<BooksVO.BookMap> books) {
            this.books = books;
        }

        public double[] getSeasonMoney() {
            return seasonMoney;
        }

        public void setSeasonMoney(double[] seasonMoney) {
            this.seasonMoney = seasonMoney;
        }

        /**
         * 添加
         * @param bookMap
         */
        public void addBook(BooksVO.BookMap bookMap){
            books.add(bookMap);
        }

        /**
         * 获取BookMap排序规则,逆序
         * @return
         */
        public Comparator<BooksVO.BookMap> getComparator(){
            Comparator<BooksVO.BookMap> comparator = new Comparator<BooksVO.BookMap>() {
                @Override
                public int compare(BooksVO.BookMap o1, BooksVO.BookMap o2) {
                    if(o1.getNum() != o2.getNum()){
                        return o2.getNum() - o1.getNum();
                    }else if(o1.getTitle()!=o2.getTitle()){
                        return o2.getTitle().compareTo(o1.getTitle());
                    }
                    return 0;
                }
            };
            return comparator;
        }

        /**
         * 排序,返回前三
         */
        public void sort(){
            Collections.sort(books,getComparator());
            List<BooksVO.BookMap> temp = new ArrayList<>();
            int i=0;
            for (BooksVO.BookMap b:books){
                temp.add(b);
                i++;
                if(i==3){
                    break;
                }
            }
            books.clear();
            books.addAll(temp);
        }
    }

    /**
     * 管理员统计数据
     */
    public static class Manager{
        //[{username:”店铺名”,money:营业额}，{username:”店铺名”,money:营业额}]，{username:”店铺名”,money:营业额}]，{username:”店铺名”,money:营业额}]，{username:”店铺名”,money:营业额}]，{username:”店铺名”,money:营业额}]]
        private List<UserVO.UserMap> stores = new ArrayList<>();// //营业额前6的店铺
        private double[] seasonMoney = new double[4];//// [第一季度，第二季度，第三季度，第四季度]//网站季度营业额

        public List<UserVO.UserMap> getStores() {
            return stores;
        }

        public void setStores(List<UserVO.UserMap> stores) {
            this.stores = stores;
        }

        public double[] getSeasonMoney() {
            return seasonMoney;
        }

        public void setSeasonMoney(double[] seasonMoney) {
            this.seasonMoney = seasonMoney;
        }

        /**
         * 添加店铺销售记录
         * @param userMap
         */
        public void addStore(UserVO.UserMap userMap){
            stores.add(userMap);
        }

        /**
         * 获取UserVO.UserMap排序规则,逆序
         * @return
         */
        public Comparator<UserVO.UserMap> getComparator(){
            Comparator<UserVO.UserMap> comparator = new Comparator<UserVO.UserMap>() {
                @Override
                public int compare(UserVO.UserMap o1, UserVO.UserMap o2) {
                    if(o1.getMoney() != o2.getMoney()){
                        return (int)(o2.getMoney() - o1.getMoney());
                    }else if(o1.getUsername()!=o2.getUsername()){
                        return o2.getUsername().compareTo(o1.getUsername());
                    }
                    return 0;
                }
            };
            return comparator;
        }

        /**
         * 排序,返回前前六
         */
        public void sort(){
            Collections.sort(stores,getComparator());
            List<UserVO.UserMap> temp = new ArrayList<>();
            int i=0;
            for (UserVO.UserMap u:stores){
                temp.add(u);
                i++;
                if(i==6){
                    break;
                }
            }
            stores.clear();
            stores.addAll(temp);
        }
    }
}
