package com.cyzz.bookstore.modules.model;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
public class Book extends Model<Book> {

    private static final long serialVersionUID = 1L;

	public Book(Long bookId, String title, String intro, String type, Long bookstoreId, String imageUrl, Double price, String author, String publisher, Date publishDate, Integer salesVolume, Integer stock, Integer isShow) {
		this.bookId = bookId;
		this.title = title;
		this.intro = intro;
		this.type = type;
		this.bookstoreId = bookstoreId;
		this.imageUrl = imageUrl;
		this.price = price;
		this.author = author;
		this.publisher = publisher;
		this.publishDate = publishDate;
		this.salesVolume = salesVolume;
		this.stock = stock;
		this.isShow = isShow;
	}

	public Book(){

	}


	@TableId("book_id")
	private Long bookId;
    /**
     * 书名
     */
	private String title;
    /**
     * 简介
     */
	private String intro;
    /**
     * 图书类型
     */
	private String type;
    /**
     * 书店名
     */
	@TableField("bookstore_id")
	private Long bookstoreId;
    /**
     * 图片链接
     */
	@TableField("image_url")
	private String imageUrl;
    /**
     * 售价
     */
	private Double price;
    /**
     * 作者
     */
	private String author;
    /**
     * 出版社
     */
	private String publisher;
	@TableField("publish_date")
	private Date publishDate;
    /**
     * 销售量
     */
	@TableField("sales_volume")
	private Integer salesVolume;
    /**
     * 库存
     */
	private Integer stock;
    /**
     * 是否上架（0：否，1：是）
     */
	@TableField("is_show")
	private Integer isShow;


	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getBookstoreId() {
		return bookstoreId;
	}

	public void setBookstoreId(Long bookstoreId) {
		this.bookstoreId = bookstoreId;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public Integer getSalesVolume() {
		return salesVolume;
	}

	public void setSalesVolume(Integer salesVolume) {
		this.salesVolume = salesVolume;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public Integer getIsShow() {
		return isShow;
	}

	public void setIsShow(Integer isShow) {
		this.isShow = isShow;
	}

	@Override
	protected Serializable pkVal() {
		return this.bookId;
	}

	@Override
	public String toString() {
		return "Book{" +
			"bookId=" + bookId +
			", title=" + title +
			", intro=" + intro +
			", type=" + type +
			", bookstoreId=" + bookstoreId +
			", imageUrl=" + imageUrl +
			", price=" + price +
			", author=" + author +
			", publisher=" + publisher +
			", publishDate=" + publishDate +
			", salesVolume=" + salesVolume +
			", stock=" + stock +
			", isShow=" + isShow +
			"}";
	}
}
