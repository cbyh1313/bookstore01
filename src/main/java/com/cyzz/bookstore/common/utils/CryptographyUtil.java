package com.cyzz.bookstore.common.utils;

/**
 * Created by 13296 on 2017/9/5.
 */

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import sun.security.provider.MD5;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.security.MessageDigest;

/**
 * 采用MD5加密解密
 * @author tfq
 * @datetime 2011-10-13
 */
public class CryptographyUtil {

    /***
     * MD5加码 生成32位md5码
     */
    public static String string2MD5(String inStr){
        MessageDigest md5 = null;
        try{
            md5 = MessageDigest.getInstance("MD5");
        }catch (Exception e){
            System.out.println(e.toString());
            e.printStackTrace();
            return "";
        }
        char[] charArray = inStr.toCharArray();
        byte[] byteArray = new byte[charArray.length];

        for (int i = 0; i < charArray.length; i++)
            byteArray[i] = (byte) charArray[i];
        byte[] md5Bytes = md5.digest(byteArray);
        StringBuffer hexValue = new StringBuffer();
        for (int i = 0; i < md5Bytes.length; i++){
            int val = ((int) md5Bytes[i]) & 0xff;
            if (val < 16)
                hexValue.append("0");
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();

    }

    /**
     * 加密解密算法 执行一次加密，两次解密
     */
    public static String convertMD5(String inStr){

        char[] a = inStr.toCharArray();
        for (int i = 0; i < a.length; i++){
            a[i] = (char) (a[i] ^ 't');
        }
        String s = new String(a);
        return s;

    }

//    // 测试主函数
//    public static void main(String args[]) {
//        String s = new String("3,"+new Date().getTime());
//        System.out.println("原始：" + s);
//        System.out.println("MD5后：" + string2MD5(s));
//        System.out.println("k:"+convertMD5(s));
//        System.out.println("解密的：" + convertMD5(convertMD5(s)));
//    }

    // 测试主函数
    public static void main(String args[]) {
        String resetPasswordToken = encodeResetPasswordToken(2l,false);
        decodeResetPasswordToken("EXEADLFABAMDF@DXD");
    }

//
//    /**
//     * token解密
//     * @param resetPasswordToken
//     * @return
//     */
//    public static ArrayList decodeResetPasswordToken(String resetPasswordToken){
//        System.out.println("加密："+resetPasswordToken);
//        resetPasswordToken = convertMD5(resetPasswordToken);
//        System.out.println("解密后："+resetPasswordToken);
//        String[] str = resetPasswordToken.split(",");
//        Long id = Long.parseLong(str[0]);
//        Long dateTime = Long.parseLong(str[1]);
//        ArrayList list  = new ArrayList();
//        list.add(id);
//        list.add(dateTime);
//        System.out.println(list.get(1));
//        System.out.println("id:"+list.get(0));
//        System.out.println("dateTime:"+dateTime);
//        return list;
//    }

    /**
     * token解密
     * @param resetPasswordToken
     * @return
     */
    public static ArrayList decodeResetPasswordToken(String resetPasswordToken){
        resetPasswordToken = convertMD5(resetPasswordToken);
        System.out.println("解密后："+resetPasswordToken);
        String[] str = resetPasswordToken.split(",");
        Long id = Long.parseLong(str[0]);
        Long dateTime = Long.parseLong(str[1]);
        int temp = Integer.parseInt(str[2]);
        boolean isAdmin;
        if(temp==1){
            isAdmin = true;
        }else {
            isAdmin = false;
        }
        ArrayList list  = new ArrayList();
        list.add(id);
        list.add(dateTime);
        list.add(isAdmin);
        System.out.println("id:"+list.get(0));
        System.out.println("dateTime:"+dateTime);
        System.out.println("isAdmin:"+isAdmin);
        return list;
    }

    /**
     * token加密
     * @param id
     * @return
     */
    public static String encodeResetPasswordToken(Long id,boolean isAdmin){
        int temp;
        if(isAdmin){
            temp = 1;
        }else {
            temp = 0;
        }
        String resetPasswordToken = id+","+new Date().getTime()+","+temp;
        System.out.println("加密前："+resetPasswordToken);
        resetPasswordToken = convertMD5(resetPasswordToken);
        System.out.println("加密后："+resetPasswordToken);
        return resetPasswordToken;
    }

}
