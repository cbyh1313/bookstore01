package test;

import com.baomidou.mybatisplus.plugins.Page;
import com.cyzz.bookstore.common.json.OrderFormVO;
import com.cyzz.bookstore.modules.model.Book;
import com.cyzz.bookstore.modules.model.Cart;
import com.cyzz.bookstore.modules.model.Commet;
import com.cyzz.bookstore.modules.model.OrderForm;
import com.cyzz.bookstore.modules.service.IBookService;
import com.cyzz.bookstore.modules.service.ICartService;
import com.cyzz.bookstore.modules.service.ICommetService;
import com.cyzz.bookstore.modules.service.IOrderFormService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by 13296 on 2018/5/30.
 */
public class TestMybatis {
//    @Test
//    public void testMapper() throws Exception{
//        ApplicationContext ctx = new ClassPathXmlApplicationContext( "classpath:spring-context.xml");
//        Page<Book> page = new Page<>(1,1);
//        IBookService bookService = ctx.getBean(IBookService.class);
//        Long id = 1l;
//        Book book = bookService.selectBookById(id);
//        System.out.println(book);
//        Page<Book> p = bookService.selectBookPage(page);
//        System.out.println(p.getPages());
//        List<Book> list = p.getRecords();
//        for(Book b:list){
//            System.out.println(b);
//        }
//        p = bookService.selectLike(page,"Java");
//        list = p.getRecords();
//        for(Book b:list){
//            System.out.println(b);
//        }
//    }

    @Test
    public void testCart() throws Exception{
        ApplicationContext ctx1 = new ClassPathXmlApplicationContext( "classpath:spring-context.xml");
        ICartService cartService = ctx1.getBean(ICartService.class);
        List<Cart> list = cartService.selectCartByUserId(1L);
        for(Cart cart:list){
            System.out.println(cart.getBookId());
        }

    }

//    @Test
//    public void testCommet() throws Exception{
//        ApplicationContext ctx1 = new ClassPathXmlApplicationContext( "classpath:spring-context.xml");
//        ICommetService commetService = ctx1.getBean(ICommetService.class);
//        List<Commet> list = commetService.selectCommetByBookId(1L);
//
//        for(Commet c:list){
//            System.out.println(c);
//        }
//    }
//
//    @Test
//    public void testDateFormChange() throws Exception{
//        OrderFormVO orderFormVO = new OrderFormVO();
//        String time = orderFormVO.changeDate();
//        System.out.println(time);
//    }
//  @Test
//    public void testOrderForm() throws Exception{
//      ApplicationContext ctx1 = new ClassPathXmlApplicationContext( "classpath:spring-context.xml");
//      IOrderFormService orderFormService  = ctx1.getBean(IOrderFormService.class);
//      List<OrderForm> list = orderFormService.selectOrderFormByStoreId(1L);
//      System.out.println(list.size());
//
//    }

}
