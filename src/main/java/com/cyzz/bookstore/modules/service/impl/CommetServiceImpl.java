package com.cyzz.bookstore.modules.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.cyzz.bookstore.common.myException.DBException;
import com.cyzz.bookstore.modules.model.Commet;
import com.cyzz.bookstore.modules.mapper.CommetMapper;
import com.cyzz.bookstore.modules.service.ICommetService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
@Service
public class CommetServiceImpl extends ServiceImpl<CommetMapper, Commet> implements ICommetService {
    @Autowired
    private  CommetMapper commetMapper;

    @Override
    public List<Commet> selectCommetByBookId(Long bookId) throws DBException {
//        System.out.println(bookId);
        EntityWrapper<Commet> ew = new EntityWrapper<>();
        ew.setEntity(new Commet());
        ew.eq("book_id",bookId).orderBy("comment_id",false);
        List<Commet> list = commetMapper.selectList(ew);
        return list;
    }

    @Override
    public List<Integer> selectCountByStar(Long bookId) throws DBException {
        List<Commet> list = selectCommetByBookId(bookId);
        List<Integer> result = new ArrayList<>();
        Integer[] is = new Integer[5];
        for(int i = 0;i<5;i++){
            is[i]=0;
        }
        for(Commet c:list){
            switch (c.getStar()){
                case 1: is[0]++ ;break;
                case 2: is[1]++ ;break;
                case 3: is[2]++ ;break;
                case 4: is[3]++ ;break;
                case 5: is[4]++ ;break;
                default:

            }
        }
        for(int i=0;i<5;i++){
            result.add(is[i]);
        }
        return result;
    }

    @Override
    public Integer insertCommet(Commet commet) throws DBException {
        Integer i = commetMapper.insert(commet);
        return i;
    }

    @Override
    public Integer updateCommet(Commet commet) throws DBException {
        System.out.println(commet.getContext()+" sdfsdfsdf");
        Integer i = commetMapper.updateAllColumnById(commet);
        return i;
    }

    @Override
    public Commet selectCommetByBookIdAndUserId(Long bookId, Long userId) throws DBException {
        Commet commet = new Commet();
        commet.setBookId(bookId);
        commet.setUserId(userId);
        commet = commetMapper.selectOne(commet);
        return commet;
    }
}
