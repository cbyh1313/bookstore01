package com.cyzz.bookstore.modules.service;

import com.cyzz.bookstore.common.myException.DBException;
import com.cyzz.bookstore.modules.model.OrderForm;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
public interface IOrderFormService extends IService<OrderForm> {
    /**
     * 根据userId查询订单列表,该方法已经失效
      * @param userId
     * @return
     * @throws DBException
     */
//    List<OrderForm> selectOrderFormByUserId(Long userId)throws DBException;

    /**
     * 更新订单
     * @param orderForm
     * @return
     * @throws DBException
     */
    Integer updataOrderForm(OrderForm orderForm)throws DBException;

    /**
     * 插入订单
     * @param orderForm
     * @return
     * @throws DBException
     */
    Integer insertOrderForm(OrderForm orderForm)throws DBException;

    /**
     * 根据顾客userid查找订单
     * @param userId
     * @return
     * @throws DBException
     */
    List<OrderForm> selectOrderFormByClientId(Long userId)throws DBException;

    /**
     * 根据店铺userid查找订单
     * @param userId
     * @return
     * @throws DBException
     */
    List<OrderForm> selectOrderFormByStoreId(Long userId)throws DBException;

    /**
     * 查找所有订单
     * @return
     * @throws DBException
     */
    List<OrderForm> selectAllOrderForm()throws DBException;

}
