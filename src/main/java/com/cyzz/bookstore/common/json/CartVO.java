package com.cyzz.bookstore.common.json;

import com.cyzz.bookstore.modules.model.Cart;

import java.util.List;

public class CartVO {
    private Long cartId;
    private Long userId;
    private Long bookId;
    private int purchaseCount;

    public Long getCartId() {
        return cartId;
    }

    public void setCartId(Long cartId) {
        this.cartId = cartId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public int getPurchaseCount() {
        return purchaseCount;
    }

    public void setPurchaseCount(int purchaseCount) {
        this.purchaseCount = purchaseCount;
    }

    public CartVO(Long cartId,Long userId,Long bookId,int putchaseCount){
        this.cartId = cartId;
        this.userId = userId;
        this.bookId = bookId;
        this.purchaseCount = putchaseCount;
    }

    public CartVO(){
    }

    public static class GetCart{
        private Long userid;
        private Long cartid;
        private Long bookid;
        private Integer purchasecount;

        public Long getUserid() {
            return userid;
        }

        public void setUserid(Long userid) {this.userid = userid;}

        public Long getCartid() {
            return cartid;
        }

        public void setCartid(Long cartid) {
            this.cartid = cartid;
        }

        public Long getBookid() {
            return bookid;
        }

        public void setBookid(Long bookid) {
            this.bookid = bookid;
        }

        public Integer getPurchasecount() {
            return purchasecount;
        }

        public void setPurchasecount(Integer purchasecount) {
            this.purchasecount = purchasecount;
        }
    }

    public static class GetWholeCart{
        private Long cartid;
        private Long bookid;
        private String imageUrl;
        private String title;
        private Double price;
        private String author;
        private String bookStore;
        private Integer number;

        public Long getCartid() {
            return cartid;
        }

        public void setCartid(Long cartid) {
            this.cartid = cartid;
        }

        public Long getBookid() {
            return bookid;
        }

        public void setBookid(Long bookid) {
            this.bookid = bookid;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getBookStore() {
            return bookStore;
        }

        public void setBookStore(String bookStore) {
            this.bookStore = bookStore;
        }

        public Integer getNumber() {
            return number;
        }

        public void setNumber(Integer number) {
            this.number = number;
        }
    }

    public static class AddCart{//单条数据加入购物车
        private Long userid;
        private Long bookid;

        public Long getUserid() {
            return userid;
        }

        public void setUserid(Long userid) {
            this.userid = userid;
        }

        public Long getBookid() {
            return bookid;
        }

        public void setBookid(Long bookid) {
            this.bookid = bookid;
        }
    }

    public static class CartForSave{
        private Long cartid;
        private Integer number;

        public Long getCartid() {
            return cartid;
        }

        public void setCartid(Long cartid) {
            this.cartid = cartid;
        }

        public Integer getNumber() {
            return number;
        }

        public void setNumber(Integer number) {
            this.number = number;
        }
    }
    public static class SaveCart{//保存顾客的购物车
        private List<CartForSave> baskets;
        private Long userid;

        public List<CartForSave> getBaskets() {
            return baskets;
        }

        public void setBaskets(List<CartForSave> baskets) {
            this.baskets = baskets;
        }

        public Long getUserid() {
            return userid;
        }

        public void setUserid(Long userid) {
            this.userid = userid;
        }
    }
}
