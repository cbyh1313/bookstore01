package com.cyzz.bookstore.modules.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.cyzz.bookstore.common.myException.DBException;
import com.cyzz.bookstore.modules.model.OrderForm;
import com.cyzz.bookstore.modules.mapper.OrderFormMapper;
import com.cyzz.bookstore.modules.service.IOrderFormService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
@Service
public class OrderFormServiceImpl extends ServiceImpl<OrderFormMapper, OrderForm> implements IOrderFormService {
    @Autowired
    private OrderFormMapper orderFormMapper;

//    @Override
//    public List<OrderForm> selectOrderFormByUserId(Long userId) throws DBException {
//        EntityWrapper<OrderForm> ew = new EntityWrapper<>();
//        ew.setEntity(new OrderForm());
//        ew.eq("user_id",userId).orderBy("order_number",false);
//        List<OrderForm> list = orderFormMapper.selectList(ew);
//        return list;
//    }

    @Override
    public Integer updataOrderForm(OrderForm orderForm) throws DBException {
        Integer i = orderFormMapper.updateAllColumnById(orderForm);
        return i;
    }

    @Override
    public Integer insertOrderForm(OrderForm orderForm) throws DBException {
        Integer i = orderFormMapper.insert(orderForm);
        return i;
    }

    @Override
    public List<OrderForm> selectOrderFormByClientId(Long userId) throws DBException {
        EntityWrapper<OrderForm> ew = new EntityWrapper<>();
        ew.setEntity(new OrderForm());
        ew.eq("client_id",userId).orderBy("order_number",false);
        List<OrderForm> list = orderFormMapper.selectList(ew);
        return list;
    }

    @Override
    public List<OrderForm> selectOrderFormByStoreId(Long userId) throws DBException {
        EntityWrapper<OrderForm> ew = new EntityWrapper<>();
        ew.setEntity(new OrderForm());
        ew.eq("store_id",userId).orderBy("order_number",false);
        List<OrderForm> list = orderFormMapper.selectList(ew);
        return list;
    }

    @Override
    public List<OrderForm> selectAllOrderForm() throws DBException {
        EntityWrapper<OrderForm> ew = new EntityWrapper<>();
        ew.setEntity(new OrderForm());
        ew.orderBy("order_number",false);
        List<OrderForm> list = orderFormMapper.selectList(ew);
        return list;
    }
}
