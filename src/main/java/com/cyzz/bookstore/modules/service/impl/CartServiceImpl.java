package com.cyzz.bookstore.modules.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.cyzz.bookstore.common.myException.DBException;
import com.cyzz.bookstore.modules.model.Book;
import com.cyzz.bookstore.modules.model.Cart;
import com.cyzz.bookstore.modules.mapper.CartMapper;
import com.cyzz.bookstore.modules.service.ICartService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
@Service
public class CartServiceImpl extends ServiceImpl<CartMapper, Cart> implements ICartService {

    @Autowired
    private CartMapper cartMapper;

    @Override
    public List<Cart> selectCartByUserId(Long userId) throws DBException {
        EntityWrapper<Cart> ew = new EntityWrapper<>();
        ew.setEntity(new Cart());
        ew.eq("user_id",userId).orderBy("cart_id",false);
        List<Cart> list = cartMapper.selectList(ew);
        return list;
    }

    @Override
    public Integer insertCart(Cart cart) throws DBException {
        Integer i = cartMapper.insert(cart);
        return i;
    }

    @Override
    public Integer delectCartByCartId(Long cartId) throws DBException {
        Integer i = cartMapper.deleteById(cartId);
        return i;
    }

    @Override
    public Integer updateCate(Cart cart) throws DBException {
        Integer i = cartMapper.updateAllColumnById(cart);
        return i;
    }
}
