package com.cyzz.bookstore.common.myException;

/**
 * Created by 13296 on 2017/8/8.
 */
public class MyException extends RuntimeException {
    public MyException(){
        super();
    }
    public MyException(String message) {
        super(message);
    }

    public MyException(String message, Throwable cause) {
        super(message, cause);
    }
}
