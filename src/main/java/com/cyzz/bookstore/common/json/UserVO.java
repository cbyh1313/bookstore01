package com.cyzz.bookstore.common.json;

import com.cyzz.bookstore.modules.model.User;

import java.util.Date;

/**
 * Created by 13296 on 2018/6/8.
 */
public class UserVO {
    private Long userId;
    private String userName;
    private String telephone;
    private String passwd;
    private Integer userType;
    private String address;

    public UserVO(){

    }
    public UserVO(Long userId, String userName, String telephone, String passwd, Integer userType, String address) {
        this.userId = userId;
        this.userName = userName;
        this.telephone = telephone;
        this.passwd = passwd;
        this.userType = userType;
        this.address = address;
    }



    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public static class GetLoginForm{
        private String username;
        private String password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    public static class CurrentUser{
        private Long userid;//:1,
        private String username;//:””,
        private String phone;//:””,
        private Integer type;//:0/1/2,  2是管理员
        private String address;//:””,

        public Long getUserid() {
            return userid;
        }

        public void setUserid(Long userid) {
            this.userid = userid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public static CurrentUser getCurrentUser(User u){
            CurrentUser currentUser = new CurrentUser();
            currentUser.setAddress(u.getAddress());
            currentUser.setPhone(u.getTelephone());
            currentUser.setType(u.getUserType());
            currentUser.setUserid(u.getUserId());
            currentUser.setUsername(u.getUserName());
            return currentUser;
        }
    }

    public static class GetRegisterForm{
        private String username;
        private String password;
        private String repassword;
        private String phone;
        private String address;
        private Integer type;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public String getRepassword() {
            return repassword;
        }

        public void setRepassword(String repassword) {
            this.repassword = repassword;
        }

        public User getUser(){
            User user = new User();
            user.setUserName(username);
            user.setUserId(new Date().getTime());
            user.setAddress(address);
            user.setPasswd(password);
            user.setTelephone(phone);
            user.setUserType(type);
            return user;
        }
    }

    /**
     * 管理员获取所有用户信息
     */
    public static class UserForAdimin{
        private Long userid;//用户id
        private String username;//用户名
        private Integer type;//用户类型
        private Integer status;////当前状态

        public Long getUserid() {
            return userid;
        }

        public void setUserid(Long userid) {
            this.userid = userid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }
    }

    /**
     * 店主数据统计
     */
    public static class UserMap{
        private String username;
        private Double money = 0.0;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public Double getMoney() {
            return money;
        }

        public void setMoney(Double money) {
            this.money = money;
        }
    }

}
