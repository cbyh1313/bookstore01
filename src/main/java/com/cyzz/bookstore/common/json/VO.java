package com.cyzz.bookstore.common.json;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: Y
 * Date: 2017-09-12
 * Time: 11:06
 */
public interface VO {

    /**
     * 分割ids字符串
     * @param idsString *
     * @return *
     */
    static Long[] divideLongIds(String idsString) {
        if (idsString == null || idsString.equals(""))
            return null;
        String[] idsTemp = idsString.split(",");
        Long[] ids = new Long[idsTemp.length];
        //System.out.println("length："+idsTemp.length);
        for(int i = 0;i<idsTemp.length;i++){
            ids[i] = Long.parseLong(idsTemp[i].trim());
        }
        return ids;
    }

    /**
     * 分割ids字符串
     * @param idsString *
     * @return *
     */
    static List<Long> divideLongIdList(String idsString) {
        List<Long> longs = new ArrayList<>();
        Collections.addAll(longs, divideLongIds(idsString));
        return longs;
    }

    /**
     * 分割ids字符串
     * @param idsString *
     * @return *
     */
    static Integer[] divideIntegerIds(String idsString) {
        if (idsString == null || idsString.equals(""))
            return null;
        String[] idsTemp = idsString.split(",");
        Integer[] ids = new Integer[idsTemp.length];
        //System.out.println("length："+idsTemp.length);
        for(int i = 0;i<idsTemp.length;i++){
            ids[i] = Integer.parseInt(idsTemp[i].trim());
        }
        return ids;
    }

    /**
     * 分割ids字符串
     * @param idsString *
     * @return *
     */
    static List<Integer> divideIntegerIdList(String idsString) {
        List<Integer> integers = new ArrayList<>();
        Collections.addAll(integers, divideIntegerIds(idsString));
        return integers;
    }

    /**
     * 合并id数组
     * @param ids *
     * @return *
     */
    static String mergeIds(Number[] ids){
        if (ids == null || ids.length == 0)
            return null;
        String idsString = "";
        idsString += ids[0];
        String separator = ",";
        for(int i = 1;i<ids.length;i++){
            idsString += separator+ids[i];
        }
        //System.out.println(idsString);
        return idsString;
    }

    /**
     * 合并id数组
     * @param ids *
     * @return *
     */
     static String mergeIds(List<? extends Number> ids){
        if (ids ==null || ids.size() == 0)
            return null;
        String idsString = "";
        idsString += ids.get(0);
        String separator = ",";
        for(int i = 1, j =ids.size(); i < j;i++){
            idsString += separator+ids.get(i);
        }
        //System.out.println(idsString);
        return idsString;
    }
}
