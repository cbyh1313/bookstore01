package com.cyzz.bookstore.common.json;

/**
 * @description：操作结果集
 * @author：
 * @date：
 */
public class Result {
    public static final int SUCCESS = 0;

    public static final int NEW_SHARE_FAILURE = 1;
    public static final int DELETE_SHARE_FAILURE = 2;
    public static final int EXAMINE_FAILURE = 3;
    public static final int PRAISE_FAILURE = 4;
    public static final int HIDE_FAILURE = 5;
    public static final int COLLECT_FAILURE = 6;
    public static final int STAR_FAILURE = 7;

    private static final long serialVersionUID = 5576237395711742681L;

    private boolean success = false;
    private Object obj = null;

    //     */
    //     * 关注操作失败:STAR_UPDATE_FAILURE ;
    //     * 收藏操作失败:COLLECT_UPDATE_FAILURE ;
    //     * 不感兴趣操作失败:HIDE_UPDATE_FAILURE ;
    //     * 赞操作失败:PRAISE_SELECT_FAILURE ;
    //     * 审核提交失败:EXAMINE_INSERT_FAILURE ;
    //     * 分享删除失败:SHARE_DELETE_FAILURE ;
    //     * 分享插入失败:SHARE_INSERT_FAILURE ;
    //     * 成功:SUCCESS ;
    //    /**
    //    private int code = SUCCESS;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

}
