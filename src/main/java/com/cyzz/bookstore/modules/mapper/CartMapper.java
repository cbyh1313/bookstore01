package com.cyzz.bookstore.modules.mapper;

import com.cyzz.bookstore.modules.model.Cart;
import com.cyzz.bookstore.common.base.MyBaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
public interface CartMapper extends MyBaseMapper<Cart> {

}