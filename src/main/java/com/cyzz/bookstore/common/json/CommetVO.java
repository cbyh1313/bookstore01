package com.cyzz.bookstore.common.json;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.cyzz.bookstore.modules.model.Commet;

import java.util.Date;

public class CommetVO {

    private Long commentId;
    private Long userId;
    private Long bookId;
    private Integer star;
    private String context;

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Integer getStar() {
        return star;
    }

    public void setStar(Integer star) {
        this.star = star;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public CommetVO(Long commentId, Long userId, Long bookId, Integer star, String context) {
        this.commentId = commentId;
        this.userId = userId;
        this.bookId = bookId;
        this.star = star;
        this.context = context;
    }

    public CommetVO(){}

    public static class GetCommetForm{
        private Long commentid;
        private Long userid;
        private Long bookid;
        private Integer start;
        private String comment;
        private Long date;

        public Long getCommentid() {return commentid; }

        public void setCommentid(Long commentid) { this.commentid = commentid; }

        public Long getUserid() {
            return userid;
        }

        public void setUserid(Long userid) {
            this.userid = userid;
        }

        public Long getBookid() {
            return bookid;
        }

        public void setBookid(Long bookid) {
            this.bookid = bookid;
        }

        public Integer getStart() {
            return start;
        }

        public void setStart(Integer start) {
            this.start = start;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public Long getDate() {
            return date;
        }

        public void setDate(Long date) {
            this.date = date;
        }

        public Commet getCommetObj(){
            Commet c = new Commet();
            c.setCommentId(commentid);
            c.setUserId(userid);
            c.setBookId(bookid);
            c.setStar(start);
            c.setContext(comment);
           return c;
        }
    }

    /**
     * 获取书本的评论
     */
    public static class CommentForBook{
        private  String username;//评价人姓名
        private Integer start;//几星 （返回字符串数字，如5）
        private String intro;//评语
        private String date;//2018-06-12

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public Integer getStart() {
            return start;
        }

        public void setStart(Integer start) {
            this.start = start;
        }

        public String getIntro() {
            return intro;
        }

        public void setIntro(String intro) {
            this.intro = intro;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }

    /**
     * 返回用户的对某书的评论
     */
    public static class CommentForUser{
        private Long commentid;//:“评论id”
        private String imgUrl;//：“”
        private String title;//：“书名“
        private String author;//：“作者‘
        private Integer start;//:”几星” （如5）
        private String comment;//:”评语”
        private String date;//:”下单日期“ (yyyy-mm-dd)

        public Long getCommentid() {
            return commentid;
        }

        public void setCommentid(Long commentid) {
            this.commentid = commentid;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public Integer getStart() {
            return start;
        }

        public void setStart(Integer start) {
            this.start = start;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }

}
