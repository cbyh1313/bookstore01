package com.cyzz.bookstore.common.json;

import com.cyzz.bookstore.modules.model.OrderForm;
import org.springframework.core.annotation.Order;

import javax.xml.bind.SchemaOutputResolver;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class OrderFormVO {
    private Long orderNumber;
    private Long clientId;
    private Long storeId;
    private Long bookId;
    private Date purchaseDate;
    private Integer purchaseCount;
    private Double price;
    private Integer isDeal;

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public int getPurchaseCount() {
        return purchaseCount;
    }

    public void setPurchaseCount(Integer purchaseCount) {
        this.purchaseCount = purchaseCount;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getIsDeal() {
        return isDeal;
    }

    public void setIsDeal(int isDeal) {
        this.isDeal = isDeal;
    }

    public OrderFormVO(){}

    public OrderFormVO(Long orderNumber,Long clientId, Long storeId,Long bookId,Date purchaseDate,Integer purchaseCount,Double price,Integer isDeal){
        this.orderNumber = orderNumber;
        this.clientId = clientId;
        this.storeId = storeId;
        this.bookId = bookId;
        this.purchaseDate = purchaseDate;
        this.purchaseCount = purchaseCount;
        this.price = price;
        this.isDeal = isDeal;
    }

    public static class GetOrderForm{
        private Long userid;
        private Long bookid;
        private Integer number;
        private String bookstore;
        private Long date;

        public Long getUserid() {
            return userid;
        }

        public void setUserid(Long userid) {
            this.userid = userid;
        }

        public Long getBookid() {
            return bookid;
        }

        public void setBookid(Long bookid) {
            this.bookid = bookid;
        }

        public Integer getNumber() {
            return number;
        }

        public void setNumber(Integer number) {
            this.number = number;
        }

        public String getBookstore() {
            return bookstore;
        }

        public void setBookstore(String bookstore) {
            this.bookstore = bookstore;
        }

        public Long getDate() {
            return date;
        }

        public void setDate(Long date) {
            this.date = date;
        }
    }

    public String changeDateToString(){//yyyy-MM-dd形式的字符串
        //Date date =new Date(1529200308040L);
        GetOrderForm orderForm = new GetOrderForm();
        Date date =new Date(orderForm.getDate());
        //Date date =new Date(1529200308040L);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("sdf:" + sdf);
        String time=sdf.format(date);//将Date对象转化
        return time;
    }

    public Date changeToDate(){//转化为date格式
        Calendar c = Calendar.getInstance();
        GetOrderForm orderForm = new GetOrderForm();
        c.setTimeInMillis(orderForm.getDate());
        //c.setTimeInMillis(1529200308040L);
        Date date = c.getTime();
        return date;
    }

    public static class DealOrder{
        private Long orderNumber;
        private Long storeid;

        public Long getOrderNumber() {
            return orderNumber;
        }

        public void setOrderNumber(Long orderNumber) {
            this.orderNumber = orderNumber;
        }

        public Long getStoreid() {
            return storeid;
        }

        public void setStoreid(Long storeid) {
            this.storeid = storeid;
        }
    }

    public static class Orders{
        private Long id;
        private String title;
        private Double price;
        private Integer number;
        private String bookStore;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public Integer getNumber() {
            return number;
        }

        public void setNumber(Integer number) {
            this.number = number;
        }

        public String getBookStore() {
            return bookStore;
        }

        public void setBookStore(String bookStore) {
            this.bookStore = bookStore;
        }
    }

    public static class GetBoughtMess{
        private List<Orders> orders = new ArrayList<>();

        public List<Orders> getOrders() {
            return orders;
        }

        public void setOrders(List<Orders> orders) {
            this.orders = orders;
        }

        public void addOrder(Orders order){
            orders.add(order);
        }
    }

    public static  class bookMess{//书本数组
        private String title;
        private Integer num;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Integer getNum() {
            return num;
        }

        public void setNum(Integer num) {
            this.num = num;
        }
    }

    public static class OrderMess{//获取店铺的订单信息
        private Long orderNumber;
        private String client;
        private String date;
        private Integer isDeal;
        private bookMess books;

        public Long getOrderNumber() {
            return orderNumber;
        }

        public void setOrderNumber(Long orderNumber) {
            this.orderNumber = orderNumber;
        }

        public String getClient() {
            return client;
        }

        public void setClient(String client) {
            this.client = client;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Integer getIsDeal() {
            return isDeal;
        }

        public void setIsDeal(Integer isDeal) {
            this.isDeal = isDeal;
        }

        public bookMess getBooks() {
            return books;
        }

        public void setBooks(bookMess books) {
            this.books = books;
        }
    }

}
