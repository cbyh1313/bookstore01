package com.cyzz.bookstore.common.result;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by 13296 on 2017/9/7.
 */
public class AbuseDetailResult {
    private Long abuseId;//举报id
    private String informerAccount;//举报者账号

    private String portraitThumbPath;//头像缩略图路径
    private String account;//被举报中账户
    private String username;//用户名
    private String reason;//举报原因
    private int result;//审核结果

    private String description;//分享文字描述
    private ArrayList<String> imagesThumbPath;//分享图片路劲
    private Date shareTime;//分享时间

    private String commentContent;//评论内容
    private String commentTarget;//评论对象
    private Date commentTime;//评论时间

    public AbuseDetailResult(){

    }

    public AbuseDetailResult(Date shareTime,String portraitThumbPath, String account, String username, String description, ArrayList<String> imagesThumbPath, String reason, int result) {
        this.shareTime = shareTime;
        this.portraitThumbPath = portraitThumbPath;
        this.account = account;
        this.username = username;
        this.description = description;
        this.imagesThumbPath = imagesThumbPath;
        this.reason = reason;
        this.result = result;
    }

    public Long getAbuseId() {
        return abuseId;
    }

    public void setAbuseId(Long abuseId) {
        this.abuseId = abuseId;
    }

    public String getInformerAccount() {
        return informerAccount;
    }

    public void setInformerAccount(String informerAccount) {
        this.informerAccount = informerAccount;
    }

    public Date getShareTime() {
        return shareTime;
    }

    public void setShareTime(Date shareTime) {
        this.shareTime = shareTime;
    }

    public String getPortraitThumbPath() {
        return portraitThumbPath;
    }

    public void setPortraitThumbPath(String portraitThumbPath) {
        this.portraitThumbPath = portraitThumbPath;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getImagesThumbPath() {
        return imagesThumbPath;
    }

    public void setImagesThumbPath(ArrayList<String> imagesThumbPath) {
        this.imagesThumbPath = imagesThumbPath;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public String getCommentTarget() {
        return commentTarget;
    }

    public void setCommentTarget(String commentTarget) {
        this.commentTarget = commentTarget;
    }

    public Date getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(Date commentTime) {
        this.commentTime = commentTime;
    }
}
