package com.cyzz.bookstore.modules.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.cyzz.bookstore.common.myException.DBException;
import com.cyzz.bookstore.modules.model.User;
import com.cyzz.bookstore.modules.mapper.UserMapper;
import com.cyzz.bookstore.modules.service.IUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService{
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> selectAllUser() throws Exception {
        EntityWrapper<User> ew = new EntityWrapper<>();
        ew.setEntity(new User());
        List<User> list = userMapper.selectList(ew);
        return list;
    }

    @Override
    public Page<User> selectLike(Page<User> page, String value) throws Exception {
        return null;
    }

    @Override
    public Page<User> selectUserPage(Page<User> page) throws Exception {
        return null;
    }

    @Override
    public User selectUserByUsername(String username) throws DBException {
        User user = new User();
        user.setUserName(username);
        user = userMapper.selectOne(user);
        return user;
    }

    @Override
    public User selectUserById(Long id) throws DBException {
        User user = userMapper.selectById(id);
        return user;
    }

    @Override
    public List<User> selectUserByIds(Long[] ids) throws DBException {
        return null;
    }

    @Override
    public Integer insertUser(User user) throws DBException {
        Integer i = userMapper.insert(user);
        return i;
    }

    @Override
    public Integer updateUser(User user) throws DBException {
        Integer i = userMapper.updateAllColumnById(user);
        return i;
    }

    @Override
    public Page<User> selectStarPage(Page<User> page, Long userId) throws Exception {
        return null;
    }

    @Override
    public List<User> selectUsersByUserType(Integer userType) throws DBException {
        EntityWrapper<User> ew = new EntityWrapper<>();
        ew.setEntity(new User());
        ew.eq("user_type",userType);
        List<User> list = userMapper.selectList(ew);
        return list;
    }
}
