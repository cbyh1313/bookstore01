package com.cyzz.bookstore.modules.service;

import com.cyzz.bookstore.common.myException.DBException;
import com.cyzz.bookstore.modules.model.Cart;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
public interface ICartService extends IService<Cart> {
    /**
     * 根据userId获得购物车列表
     * @param userId
     * @return
     * @throws DBException
     */
	List<Cart> selectCartByUserId(Long userId)throws DBException;

    /**
     * 向购物车插入物品
     * @param cart
     * @throws DBException
     */
	Integer insertCart(Cart cart)throws DBException;

    /**
     * 根据cartId删除购物车物品
     * @param cartId
     * @throws DBException
     */
    Integer delectCartByCartId(Long cartId)throws DBException;

    /**
     * 更新购物车
     * @param cart
     * @throws DBException
     */
    Integer updateCate(Cart cart)throws DBException;
}
