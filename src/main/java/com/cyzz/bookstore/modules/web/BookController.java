package com.cyzz.bookstore.modules.web;


import com.baomidou.mybatisplus.plugins.Page;
import com.cyzz.bookstore.common.base.MyBaseController;
import com.cyzz.bookstore.common.json.BooksVO;
import com.cyzz.bookstore.modules.model.Book;
import com.cyzz.bookstore.modules.model.User;
import com.cyzz.bookstore.modules.service.IBookService;
import com.cyzz.bookstore.modules.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
@Controller
@RequestMapping("/book")
public class BookController extends MyBaseController {
    @Autowired
    private IBookService bookService;
    @Autowired
    private IUserService userService;

    /**
     * 获取图书的详细信息
     * @param bookId
     * @return
     */
    @RequestMapping(value = "/one", method = RequestMethod.GET)
    @ResponseBody
    public Object getOne( @RequestParam("bookId") Long bookId){
        Book book = null;
        try {
            book = bookService.selectBookById(bookId);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError("该书不存在或者已经下架！");
        }
        User bookstore = userService.selectUserById(book.getBookstoreId());
        BooksVO.BookDetail detail = new BooksVO.BookDetail();
        detail.setBookStore(bookstore.getUserName());
        detail.setAuthor(book.getAuthor());
        detail.setFromAddress(bookstore.getAddress());
        detail.setId(book.getBookId());
        detail.setImgUrl(book.getImageUrl());
        detail.setIntro(book.getIntro());
        detail.setPrice(book.getPrice());
        detail.setPublishDate(changeDate(book.getPublishDate()));
        detail.setSalesVolume(book.getSalesVolume());
        detail.setStock(book.getStock());
        detail.setTitle(book.getTitle());
        detail.setType(book.getType());
        detail.setPublisher(book.getPublisher());
        return renderSuccess(detail);
    }

    /**
     * 分页获取顾客浏览图书页面的图书列表
     * @param pageNum
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/books", method = RequestMethod.GET)
    @ResponseBody
    public Object getBooks( @RequestParam("page") int pageNum) throws Exception {
        Page<Book> page = new Page<>(pageNum,20);
        page = bookService.selectBookPage(page);
        List<Book> list = page.getRecords();
        List<BooksVO.getBookList> blist = new ArrayList<>();
        for(Book b:list){
            BooksVO.getBookList book = new BooksVO.getBookList();
            book.setId(b.getBookId());
            book.setImgUrl(b.getImageUrl());
            book.setIntro(b.getIntro());
            book.setPrice(b.getPrice());
            book.setTitle(b.getTitle());
            blist.add(book);
        }

        BooksVO.BookListAndPage bookListAndPage = new BooksVO.BookListAndPage();
        bookListAndPage.setList(blist);
        bookListAndPage.setTotalPages(page.getPages());
        return renderSuccess(bookListAndPage);
    }


    /**
     * 分页模糊查询图书列表
     * @param pageNum
     * @param value
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/searchBook", method = RequestMethod.GET)
    @ResponseBody
    public Object getSearchBooks( @RequestParam("page") int pageNum,@RequestParam("search") String value) throws Exception {
        System.out.println(pageNum+" "+value);
        Page<Book> page = new Page<>(pageNum,20);
        bookService.selectLike(page,value);
        List<Book> list = page.getRecords();
        List<BooksVO.getBookList> blist = new ArrayList<>();
        for(Book b:list){
            BooksVO.getBookList book = new BooksVO.getBookList();
            book.setId(b.getBookId());
            book.setImgUrl(b.getImageUrl());
            book.setIntro(b.getIntro());
            book.setPrice(b.getPrice());
            book.setTitle(b.getTitle());
            blist.add(book);
        }
        BooksVO.BookListAndPage bookListAndPage = new BooksVO.BookListAndPage();
        bookListAndPage.setList(blist);
        bookListAndPage.setTotalPages(page.getPages());
        return renderSuccess(bookListAndPage);
    }

    /**
     * 获取店铺所有的图书
     * @param storeId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/allOfBookstore", method = RequestMethod.GET)
    @ResponseBody
    public Object bookstore( @RequestParam("userid") Long storeId) throws Exception {
        List<Book> list = bookService.selectBooksForBookstore(storeId);
        List<BooksVO.BooksForStore> blist = new ArrayList<>();
        for(Book b:list){
            blist.add(new BooksVO.BooksForStore(b));
        }
        return renderSuccess(blist);
    }

    /**
     * 获取店铺的一个图书信息
     * @param bookId
     * @return
     */
    @RequestMapping(value = "/oneOfBookstore", method = RequestMethod.GET)
    @ResponseBody
    public Object oneOfBookstore( @RequestParam("bookid") Long bookId)  {
        Book book = null;
        try {
            book = bookService.selectBookById(bookId);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError("该书不存在！");
        }
        if(book!=null){
            BooksVO.BookForBookstore booksVO = new BooksVO.BookForBookstore(book);
            return renderSuccess(booksVO);
        }else{
            return renderError("该书不存在！");
        }
    }

    /**
     * 上架图书或者跟新图书信息
     * @param bookForBookstoreToUpdata
     * @return
     */
    @RequestMapping(value = "/upBook", method = RequestMethod.POST)
    @ResponseBody
    public Object upBook(@RequestBody BooksVO.BookForBookstoreToUpdata bookForBookstoreToUpdata){
        if(bookForBookstoreToUpdata.getId()==0){
            Book book = bookForBookstoreToUpdata.getBook(new Book());
            bookService.insert(book);
        }else{//更新
            Book book = bookService.selectBookById(bookForBookstoreToUpdata.getId());
            book = bookForBookstoreToUpdata.getBook(book);
            try {
                bookService.updataBook(book);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return renderSuccess();
    }

    /**
     * 改变图书上下架状态
     * @param bookId
     * @return
     */
    @RequestMapping(value = "/changeBookStatus", method = RequestMethod.GET)
    @ResponseBody
    public Object changeBookStatus(@RequestParam("bookid") Long bookId) {
        try {
            Book book = bookService.selectBookById(bookId);
            if(book.getIsShow()==0){
                book.setIsShow(1);
            }else{
                book.setIsShow(0);
            }
            bookService.updataBook(book);
        } catch (Exception e) {
            e.printStackTrace();
            return renderError();
        }
        return renderSuccess();
    }


}
