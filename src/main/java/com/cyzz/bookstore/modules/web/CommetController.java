package com.cyzz.bookstore.modules.web;


import com.baomidou.mybatisplus.plugins.Page;
import com.cyzz.bookstore.common.base.MyBaseController;
import com.cyzz.bookstore.common.json.CommetVO;
import com.cyzz.bookstore.common.myException.DBException;
import com.cyzz.bookstore.common.utils.DateUtils;
import com.cyzz.bookstore.modules.model.Book;
import com.cyzz.bookstore.modules.model.Commet;
import com.cyzz.bookstore.modules.model.User;
import com.cyzz.bookstore.modules.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
@Controller
@RequestMapping("/commet")
public class CommetController extends MyBaseController {
    @Autowired
    private ICommetService commetService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IBookService bookService;
    @Autowired
    private IOrderFormService orderFormService;

    /**
     * 查看商品的所有评论
     * @param bookId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/comments", method = RequestMethod.GET)
    @ResponseBody
    public Object comments( @RequestParam("bookid") Long bookId) throws Exception {
//        System.out.println(bookId);
        List<Commet> list= commetService.selectCommetByBookId(bookId);
        System.out.println(list.size());
        List<CommetVO.CommentForBook> clist = new ArrayList<>();
        for(Commet c:list){
            CommetVO.CommentForBook commentForBook = new CommetVO.CommentForBook();
            commentForBook.setDate(changeDate(new Date(c.getCommentId())));
            commentForBook.setIntro(c.getContext());
            commentForBook.setStart(c.getStar());
            User u  = userService.selectUserById(c.getUserId());
            commentForBook.setUsername(u.getUserName());
            clist.add(commentForBook);
        }
        return renderSuccess(clist);
    }

    /**
     * 跳转到插入或更新评论的页面，GET
     * @param userId
     * @param bookId
     * @return
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    @ResponseBody
    public Object getCommet(@RequestParam("userid") Long userId,@RequestParam("bookid") Long bookId) {
        Commet commet = commetService.selectCommetByBookIdAndUserId(bookId,userId);
        Book book = bookService.selectBookById(bookId);
//        orderFormService
        CommetVO.CommentForUser commentForUser = new CommetVO.CommentForUser();
        commentForUser.setAuthor(book.getAuthor());
        commentForUser.setImgUrl(book.getImageUrl());
        commentForUser.setTitle(book.getTitle());
        if(commet!=null){
            commentForUser.setComment(commet.getContext());
            commentForUser.setCommentid(commet.getCommentId());
            commentForUser.setStart(commet.getStar());
            commentForUser.setDate(DateUtils.date2String(new Date(commet.getCommentId())));
        }
        return  renderSuccess(commentForUser);
    }

    /**
     * 插入或者更新评论,POST
     * @param form
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    @ResponseBody
    public Object insert(@RequestBody CommetVO.GetCommetForm form) throws Exception {
        Commet c = form.getCommetObj();
        System.out.println(c.getCommentId()+c.getContext());
        if(c.getCommentId() == 0){
            System.out.println("插入");
            try {
                String message = "评论成功";
                c.setCommentId(new Date().getTime());
                commetService.insertCommet(c);
                return renderSuccess(message);
            } catch (Exception ex) {
                return renderSuccess(ex.getMessage());
            }
        }else{
            System.out.println("更新");
            try {
                String message = "评论成功";
                Integer i = commetService.updateCommet(c);
                System.out.println(i);
                return renderSuccess(message);
            } catch (DBException ex) {
                return renderSuccess(ex.getMessage());
            }
        }
    }



}
