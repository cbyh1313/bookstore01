package com.cyzz.bookstore.modules.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.cyzz.bookstore.common.myException.DBException;
import com.cyzz.bookstore.modules.model.Book;
import com.cyzz.bookstore.modules.mapper.BookMapper;
import com.cyzz.bookstore.modules.model.User;
import com.cyzz.bookstore.modules.service.IBookService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements IBookService {
    @Autowired
    private BookMapper bookMapper;

    @Override
    public List<Book> selectAllBooks() throws Exception {
        EntityWrapper<Book> ew = new EntityWrapper<>();
        ew.setEntity(new Book());
        ew.orderBy("book_id",false);
        List<Book> list = bookMapper.selectList(ew);
        return list;
    }

    @Override
    public Book selectBookById(Long id) throws DBException {
        Book book = bookMapper.selectById(id);
        return book;
    }

    @Override
    public void updataBook(Book book) throws Exception {
        bookMapper.updateAllColumnById(book);
    }

    @Override
    public List<Book> selectBooksForBookstore(Long storeId) throws DBException {
        EntityWrapper<Book> ew = new EntityWrapper<>();
        ew.setEntity(new Book());
        ew.eq("bookstore_id",storeId).orderBy("book_id",false);
        List<Book> list = bookMapper.selectList(ew);
        return list;
    }

    @Override
    public List<Book> seleteBooksByType(String type) throws DBException {
        EntityWrapper<Book> ew = new EntityWrapper<>();
        ew.setEntity(new Book());
        ew.eq("type",type).orderBy("book_id",false);
        List<Book> list = bookMapper.selectList(ew);
        return list;
    }

    @Override
    public Page<Book> selectLike(Page<Book> page, String value) throws Exception {
        System.out.println("value:"+value);
        EntityWrapper<Book> ew  = new EntityWrapper<>();
        if(value.trim().length()==0&&value.trim().equals("")){
            ew.where("is_show=1").orderBy("book_id");
        }else{
            ew.where("is_show=1").like("title",value)
                    .or().like("intro",value)
                    .or().like("type",value)
                    .or().like("author",value)
                    .or().like("publisher",value)
                    .orderBy("bookId",false);
        }
        page.setRecords(bookMapper.selectPage(page,ew));
        return page;
    }

    @Override
    public Page<Book> selectBookPage(Page<Book> page) throws Exception {
        EntityWrapper<Book> ew = new EntityWrapper<>();
        ew.setEntity(new Book());
        ew.where("is_show",1);
        page.setRecords(bookMapper.selectPage(page,ew));
        return page;
    }
}
