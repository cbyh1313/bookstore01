package com.cyzz.bookstore.common.utils;

//import net.coobird.thumbnailator.Thumbnails;

import net.coobird.thumbnailator.Thumbnails;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Random;

import static sun.java2d.cmm.ColorTransform.In;

/**
 * Created by 13296 on 2017/8/16.
 */
public class ImgCompressUtil {

    /**
     * 文件尺寸不变，压缩文件大小
     * @param multipartFile
     * @return
     */
    public static String getCompress(MultipartFile multipartFile){
        File dest=new File("D:\\images\\s2.png");//暂存位置
//        System.out.println(dest.getAbsoluteFile());
        try {
            Thumbnails.of(multipartFile.getInputStream()).scale(0.5f).toFile(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dest.getAbsolutePath();
    }

    /**
     * 自定义名字
     * @param originalFilename
     * @return
     */
    public static String getName(String originalFilename){
        String substring = originalFilename.substring(originalFilename.lastIndexOf(".")).toLowerCase();
        Random random = new Random();
        String name = random.nextInt(10000) + System.currentTimeMillis() + substring;
        return name;
    }


    public static void main(String[] args) {
        //创建图片文件(此处为1024×768px的图片)和处理后的图片文件
        File dest=new File("D:\\images\\s1.png");
        File dest1=new File("D:\\images\\s2.png");

    }

}
