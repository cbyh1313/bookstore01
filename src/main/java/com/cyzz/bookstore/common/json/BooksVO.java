package com.cyzz.bookstore.common.json;

import com.cyzz.bookstore.common.utils.DateUtils;
import com.cyzz.bookstore.modules.model.Book;
import org.omg.PortableInterceptor.INACTIVE;

import java.util.Date;
import java.util.List;

/**
 * Created by 13296 on 2018/6/4.
 */
public class BooksVO {


    public BooksVO(){}

    /**
     * 非登录用户浏览数的页面
     */
    public static class getBookList{
        private Long id;//书本id,
        private String imgUrl;//图片地址
        private String title;//书名
        private Double price;//价格
        private String intro;//简介

        public getBookList(){

        }

        public getBookList(Book book){
            this.id = book.getBookId();
            this.imgUrl = book.getImageUrl();
            this.title =book.getTitle();
            this.price = book.getPrice();
            this.intro = book.getIntro();
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public String getIntro() {
            return intro;
        }

        public void setIntro(String intro) {
            this.intro = intro;
        }
    }

    public static class BookListAndPage{
        private Integer totalPages;
        private List<getBookList> list;

        public Integer getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(Integer totalPages) {
            this.totalPages = totalPages;
        }

        public List<getBookList> getList() {
            return list;
        }

        public void setList(List<getBookList> list) {
            this.list = list;
        }
    }

    /**
     * 查看图书详情
     */
    public static class BookDetail{
        private Long id;
        private String type;
        private String imgUrl;
        private String title;
        private Double price;
        private String intro;
        private String publisher;
        private String publishDate;
        private String author;
        private Integer salesVolume;
        private Integer stock;
        private String bookStore;
        private String fromAddress;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public String getIntro() {
            return intro;
        }

        public void setIntro(String intro) {
            this.intro = intro;
        }

        public String getPublisher() {
            return publisher;
        }

        public void setPublisher(String publisher) {
            this.publisher = publisher;
        }

        public String getPublishDate() {
            return publishDate;
        }

        public void setPublishDate(String publishDate) {
            this.publishDate = publishDate;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public Integer getSalesVolume() {
            return salesVolume;
        }

        public void setSalesVolume(Integer salesVolume) {
            this.salesVolume = salesVolume;
        }

        public Integer getStock() {
            return stock;
        }

        public void setStock(Integer stock) {
            this.stock = stock;
        }

        public String getBookStore() {
            return bookStore;
        }

        public void setBookStore(String bookStore) {
            this.bookStore = bookStore;
        }

        public String getFromAddress() {
            return fromAddress;
        }

        public void setFromAddress(String fromAddress) {
            this.fromAddress = fromAddress;
        }

    }

    public static class BookForBookstore{
        private Long id;//:图书id，
        private String imgUrl;//：图片地址
        private String title;//：书名
        private String type;//:分类
        private String author;//:作者
        private String publisher;//:”出版社”,
        private String publishDate;//:”2018-06-02“,
        private Integer stock;//:”库存”,
        private String intro;//:”简介”
        private Double price;//：单价

        public BookForBookstore(){

        }

        /**
         * 获取店铺的单本图书信息
         * @param book
         */
        public BookForBookstore(Book book){
            this.id = book.getBookId();
            this.imgUrl = book.getImageUrl();
            this.title = book.getTitle();
            this.type = book.getType();
            this.author = book.getAuthor();
            this.publisher =book.getPublisher();
            this.publishDate = DateUtils.date2String(book.getPublishDate());
            this.stock = book.getStock();
            this.intro = book.getIntro();
            this.price = book.getPrice();
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getPublisher() {
            return publisher;
        }

        public void setPublisher(String publisher) {
            this.publisher = publisher;
        }

        public String getPublishDate() {
            return publishDate;
        }

        public void setPublishDate(String publishDate) {
            this.publishDate = publishDate;
        }

        public Integer getStock() {
            return stock;
        }

        public void setStock(Integer stock) {
            this.stock = stock;
        }

        public String getIntro() {
            return intro;
        }

        public void setIntro(String intro) {
            this.intro = intro;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }
    }

    /**
     * 上架图书(添加单本图书到店铺/修改已存在的图书信息)
     */
    public static class BookForBookstoreToUpdata extends BookForBookstore{
        private Long bookStoreId;//:书店id，
        private Integer status;  //新增===上架状态

        public Long getBookStoreId() {
            return bookStoreId;
        }

        public void setBookStoreId(Long bookStoreId) {
            this.bookStoreId = bookStoreId;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        /*获取新上架的书类*/
        public Book getBook(Book book){
            if(book.getBookId()==null){
                book.setBookId(new Date().getTime());
                book.setSalesVolume(0);
            }
            book.setIsShow(this.status);
            book.setStock(super.stock);
            book.setIntro(super.intro);
            book.setImageUrl(super.imgUrl);
            book.setPublisher(super.publisher);
            book.setPublishDate(DateUtils.string2Date(super.publishDate));
            book.setAuthor(super.author);
            book.setTitle(super.title);
            book.setPrice(super.price);
            book.setBookstoreId(bookStoreId);
            return book;
        }

    }

    /**
     * 返回店铺的所有图书对用的json
     */
    public static class BooksForStore{
        private Long id;//:图书id，
        private String imgUrl;//：图片地址
        private String title;//：书名
        private Double price;//：单价
        private Integer status;//:图书状态

        public BooksForStore(){

        }

        public BooksForStore(Book book){
            this.id = book.getBookId();
            this.imgUrl = book.getImageUrl();
            this.title = book.getTitle();
            this.price = book.getPrice();
            this.status = book.getIsShow();
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }
    }

    /**
     * 统计图书的映射
     */
    public static class BookMap{
        private String title;
        private Integer num;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Integer getNum() {
            return num;
        }

        public void setNum(Integer num) {
            this.num = num;
        }
    }
}
