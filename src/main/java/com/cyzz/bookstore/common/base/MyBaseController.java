package com.cyzz.bookstore.common.base;

import com.baomidou.mybatisplus.plugins.Page;
import com.cyzz.bookstore.common.json.Result;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

/**
 * Created by 13296 on 2017/8/12.
 */
public class MyBaseController {
    /**
     * ajax失败
     * @return {Object}
     */
    public Object renderError() {
        Result result = new Result();
        return result;
    }

    /**
     * ajax失败
     * @param obj 失败时的对象
     * @return {Object}
     */
    public Object renderError(Object obj) {
        Result result = new Result();
        result.setObj(obj);
        return result;
    }

    /**
     * ajax成功
     *
     * @return {Object}
     */
    public Object renderSuccess() {
        Result result = new Result();
        result.setSuccess(true);
        return result;
    }

    /**
     * ajax成功
     * @param obj 成功时的对象
     * @return {Object}
     */
    public Object renderSuccess(Object obj) {
        Result result = new Result();
        result.setSuccess(true);
        result.setObj(obj);
        return result;
    }

    public <T> Page<T> getPage(int current, int size, String sort, String order){
        Page<T> page = new Page<T>(current, size, sort);
        if ("desc".equals(order)) {
            page.setAsc(false);
        } else {
            page.setAsc(true);
        }
        return page;
    }

    /**
     * 获取项目basePath
     * @param request
     * @return
     */
    public String getBasePath(HttpServletRequest request){
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://"
                + request.getServerName() + ":" + request.getServerPort()
                + path;
        return basePath;
    }

    public String changeDate(Date date){//yyyy-MM-dd形式的字符串
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        String time=sdf.format(date);//将Date对象转化
        return time;
    }
}
