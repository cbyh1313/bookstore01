package com.cyzz.bookstore.modules.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.cyzz.bookstore.common.myException.DBException;
import com.cyzz.bookstore.modules.model.Book;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author CBY
 * @since 2018-05-30
 */
public interface IBookService extends IService<Book> {
    /**
     * 查询所有图书
     * @return
     * @throws Exception
     */
    List<Book> selectAllBooks()throws Exception;
    /**
     * 模糊分页查询用户
     * @param page
     * @param value
     * @return
     * @throws Exception
     */
    Page<Book> selectLike(Page<Book> page, String value)throws Exception;

    /**
     * 分页查询用户
     * @param page
     * @return
     * @throws Exception
     */
    Page<Book> selectBookPage(Page<Book> page)throws Exception;

    /**
     * 根据图书id查找
     * @param id
     * @return
     * @throws DBException
     */
    Book selectBookById(Long id)throws DBException;

    /**
     * 更新书
     * @param book
     * @return
     * @throws Exception
     */
    void updataBook(Book book)throws Exception;

    /**
     * 根据店铺id查找图书
     * @param storeId
     * @return
     * @throws DBException
     */
    List<Book> selectBooksForBookstore(Long storeId)throws DBException;

    /**
     * 根据图书类型查找图书
     * @param typr
     * @return
     * @throws DBException
     */
    List<Book> seleteBooksByType(String type)throws DBException;
}
