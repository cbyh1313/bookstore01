package com.cyzz.bookstore.common.result;

/**
 * Created by 13296 on 2017/9/7.
 */
public class CurrentUserResult {
    private Long id;
    private Integer isShare;
    private String portraitPath;//头像原图路径
    private String portraitThumbPath;//头像缩略图路径
    private String mail;//邮箱
    private String account;//账号
    private String username;//用户名

    public Integer getIsShare() {
        return isShare;
    }

    public void setIsShare(Integer isShare) {
        this.isShare = isShare;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPortraitPath() {
        return portraitPath;
    }

    public void setPortraitPath(String portraitPath) {
        this.portraitPath = portraitPath;
    }

    public String getPortraitThumbPath() {
        return portraitThumbPath;
    }

    public void setPortraitThumbPath(String portraitThumbPath) {
        this.portraitThumbPath = portraitThumbPath;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


}
