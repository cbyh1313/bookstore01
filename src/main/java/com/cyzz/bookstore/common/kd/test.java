package com.cyzz.bookstore.common.kd;


public class test {

    public static void main(String[] args) throws Exception
    {
        KdApiOrderDemo demo =new KdApiOrderDemo();
        UserForKD sender =new UserForKD();
        UserForKD receiver=new UserForKD();
        OrderForKD order1=new OrderForKD();

        order1.OrderCode="012657999997";
        order1.GoodsName="书";
        order1.Remark="小心轻放";

        sender.Name="Taylor";
        sender.Mobile="15018442396";
        sender.ProvinceName="上海";
        sender.CityName="上海";
        sender.ExpAreaName="青浦区";
        sender.Address="明珠路73号";

        receiver.Name="Yann";
        receiver.Mobile="15018442396";
        receiver.ProvinceName="北京";
        receiver.CityName="北京";
        receiver.ExpAreaName="朝阳区";
        receiver.Address="三里屯街道雅秀大厦";


        String str = "{'OrderCode': '"+ order1.OrderCode+"'," +
                "'ShipperCode':'SF'," +
                "'PayType':1," +
                "'ExpType':1," +
                "'Cost':10.0," +
                "'OtherCost':0.0," +
                "'Sender':" +
                "{" +
                "'Company':'bookstore','Name':'"+sender.Name+"','Mobile':'"+sender.Mobile+"','ProvinceName':'"+sender.ProvinceName +"','CityName':'"+sender.CityName+"','ExpAreaName':'"+sender.ExpAreaName+"','Address':'"+sender.Address+"'}," +
                "'Receiver':" +
                "{" +
                "'Company':'Person','Name':'"+receiver.Name+"','Mobile':'"+receiver.Mobile+"','ProvinceName':'"+receiver.ProvinceName+"','CityName':'"+receiver.CityName+"','ExpAreaName':'"+receiver.ExpAreaName+"','Address':'"+receiver.Address+"'}," +
                "'Commodity':" +
                "[{" +
                "'GoodsName':'"+order1.GoodsName+"','Goodsquantity':1,'GoodsWeight':1.0}]," +
                "'Weight':1.0," +
                "'Quantity':1," +
                "'Volume':0.0," +
                "'Remark':'"+order1.Remark+"'," +
                "'IsReturnPrintTemplate':1}";

        demo.sendData(order1,sender,receiver);
    }

}
