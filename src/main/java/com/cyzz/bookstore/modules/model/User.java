package com.cyzz.bookstore.modules.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author CBY
 * @since 2018-06-17
 */
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    @TableId("user_id")
	private Long userId;
	@TableField("user_name")
	private String userName;
	private String telephone;
    /**
     * 0:顾客
1：店主
2：管理员

     */
	private String passwd;
	@TableField("user_type")
	private Integer userType;
	private String address;
    /**
     * 用户状态
     */
	private Integer status;


	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	protected Serializable pkVal() {
		return this.userId;
	}

	@Override
	public String toString() {
		return "User{" +
			"userId=" + userId +
			", userName=" + userName +
			", telephone=" + telephone +
			", passwd=" + passwd +
			", userType=" + userType +
			", address=" + address +
			", status=" + status +
			"}";
	}
}
